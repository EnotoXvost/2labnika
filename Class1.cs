using System;
using System.Collections.Generic;

interface ITask
{
    string Name { get; }
    void Execute();
}

interface INotifier
{
    void AttachObserver(IObserver observer);
    void DetachObserver(IObserver observer);
    void NotifyObservers();
}

interface IObserver
{
    void Update(ITask task);
}

class Task : ITask, IObserver
{
    public string Name { get; set; }

    public Task(string name)
    {
        Name = name;
    }

    public void Execute()
    {
        Console.WriteLine("Выполнение задачи: " + Name);
    }

    public void Update(ITask task)
    {
        Console.WriteLine($"Задача '{task.Name}' выполнена");
    }
}

class Project : ITask, INotifier
{
    private List<ITask> tasks = new List<ITask>();
    private List<IObserver> observers = new List<IObserver>();

    public string Name { get; set; }

    public Project(string name)
    {
        Name = name;
    }

    public void AttachObserver(IObserver observer)
    {
        observers.Add(observer);
    }

    public void DetachObserver(IObserver observer)
    {
        observers.Remove(observer);
    }

    public void NotifyObservers()
    {
        foreach (IObserver observer in observers)
        {
            observer.Update(this);
        }
    }

    public void AddTask(ITask task)
    {
        tasks.Add(task);
    }

    public void RemoveTask(ITask task)
    {
        tasks.Remove(task);
    }

    public void Execute()
    {
        Console.WriteLine($"Выполнение операции в контейнере {Name}");

        foreach (ITask task in tasks)
        {
            task.Execute();
            NotifyObservers();
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        Task task1 = new Task("таск 1");
        Task task2 = new Task("таск 2");
        Task task3 = new Task("таск 3");


        Project project1 = new Project("Проект 1");
        project1.AddTask(task1);
        project1.AttachObserver(task1);
        project1.AddTask(task2);
        project1.AttachObserver(task2);

        Project project2 = new Project("Проект 2");
        project2.AddTask(project1);
        project2.AddTask(task3);
        project2.AttachObserver(task3);

        project2.Execute();

        Console.ReadLine();
    }
}
